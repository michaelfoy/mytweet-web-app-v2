const jwt = require('jsonwebtoken');
const data = require('../models/admin.json');
const User = require('../models/user.js');

/**
 * Creates a new json web token for a logged in user
 *
 * @param user The logged in user
 * @returns Encoded json web token
 */
exports.createToken = function(user) {
  return jwt.sign({ id: user._id, email: user.email }, data.authPassword, {
    algorithm: 'HS256',
    expiresIn: '1h',
  });
};

/**
 * Decodes a json web token
 *
 * @param token Token to be decoded
 * @returns If logged in user; object containing user's information
 */
exports.decodeToken = function(token) {
  var userInfo = {};
  try {
    var decoded = jwt.verify(token, data.authPassword);
    userInfo.userId = decoded.id;
    userInfo.email = decoded.email;
  } catch (e) {
    console.log("Unable to decode web token")
  }

  return userInfo;
};

/**
 * Validates decoded web token data against the database
 *
 * @param decoded
 * @param request
 * @param callback
 */
exports.validate = function (decoded, request, callback) {
  User.findOne({ _id: decoded.id }).then(user => {
    if (user != null) {
      callback(null, true);
    } else {
      callback(null, false);
    }
  }).catch(err => {
    callback(null, false);
  });
};