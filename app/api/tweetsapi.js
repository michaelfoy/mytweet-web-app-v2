'use strict';

const Tweet = require('../models/tweet');
const Boom = require('boom');

/**
 * Endpoint to create a new tweet
 */
exports.create = {
  auth: {
    strategy: 'jwt',
  },

  handler: function (request, reply) {
    let tweet = new Tweet(request.payload);
    tweet.tweeter = request.params.id;
    tweet.save().then(newTweet => {
      reply(newTweet._id).code(201);
    }).catch(err => {
      reply(Boom.badImplementation('error creating tweet'));
    });
  },
};

/**
 * Endpoint to retrieve all tweets
 */
exports.findAll = {
  auth: {
    strategy: 'jwt',
  },

  handler: function (request, reply) {
    Tweet.find({}).populate('tweeter').then(tweets => {
      reply(tweets);
    }).catch(err => {
      reply(Boom.badImplementation('error accessing db'));
    });
  },
};

/**
 * Endpoint to retrieve a single tweet
 */
exports.getOneTweet = {
  auth: {
    strategy: 'jwt',
  },

  handler: function (request, reply) {
    const tweetId = request.payload;

    Tweet.find({_id: tweetId}).then(foundTweet => {
      reply(foundTweet);
    }).catch(err => {
      reply(Boom.badImplementation('error accessing db'));
    });
  },
};

/**
 * Endpoint to retrieve all tweets from a single user
 */
exports.getTweetsforUser = {
  auth: {
    strategy: 'jwt',
  },

  handler: function (request, reply) {

    Tweet.find({tweeter: request.params.id}).then(tweets => {
      reply(tweets);
    }).catch(err => {
      reply(Boom.badImplementation('error accessing db'));
    });
  },
};

/**
 * Endpoint to delete a selection of tweets
 */
exports.delete = {
  auth: {
    strategy: 'jwt',
  },

  handler: function (request, reply) {
    const tweetsArray = Object.keys(request.payload);

    for (let i = 0; i <= tweetsArray.length; i++) {
      Tweet.remove({_id: tweetsArray[i]}).then(err => {
        reply().code(204);
      }).catch(err => {
        reply(Boom.badImplementation('error removing Tweets'));
      });
    }
  },
};

/**
 * Endpoint to delete all tweets from database.
 * Only to be used for test purposes
 */
exports.deleteAll = {
  auth: {
    strategy: 'jwt',
  },

  handler: function (request, reply) {
    Tweet.remove({}).then(err => {
      reply().code(204);
    }).catch(err => {
      reply(Boom.badImplementation('error removing Tweets'));
    });
  },
};