'use strict';

const User = require('../models/user');
const Boom = require('boom');
const utils = require('./utils.js');
const bcrypt = require('bcrypt-nodejs');

/**
 * Endpoint to create a new user
 */
exports.create = {
  auth: false,

  handler: function (request, reply) {
    const user = new User(request.payload);

    bcrypt.hash(user.password, saltRounds(), null, function (err, result) {
      user.password = result;
      user.save().then(newUser => {
        reply(newUser).code(201);
      }).catch(err => {
        reply(Boom.badImplementation('error creating user'));
      });
    });
  },
};

/**
 * Endpoint to retrieve a single user
 */
exports.getOne = {
  auth: {
    strategy: 'jwt',
  },

  handler: function (request, reply) {
    const userId = request.params.id;
    User.findOne({ _id : userId }).then(foundUser => {
      reply(foundUser);
    }).catch(err => {
      reply(Boom.badImplementation('error accessing db'));
    });
  },
};

/**
 * Endpoint to retrieve all users
 */
exports.getAllUsers = {

  auth: {
    strategy: 'jwt',
  },

  handler: function (request, reply) {
    User.find({}).then(users => {
      reply(users);
    }).catch(err => {
      reply(Boom.badImplementation('error accessing getting users'));
    });
  },
};

/**
 * Endpoint to delete a user
 */
exports.delete = {
  auth: {
    strategy: 'jwt',
  },

  handler: function (request, reply) {
    const usersArray = Object.keys(request.payload);

    for (let i = 0 ; i <= usersArray.length; i++) {
      User.remove({ _id: usersArray[i] }).then(err => {
        reply().code(204);
      }).catch(err => {
        reply(Boom.badImplementation('error removing Users'));
      });
    }
  },
};

/**
 * Endpoint to update a user's details
 */
exports.update = {
  auth: {
    strategy: 'jwt',
  },

  handler: function (request, reply) {
    let editedUser = request.payload;
    const userId = request.params.id;

    let plainPassword = editedUser.password;

    User.findOne({ _id : userId }).then(user => {
      if (!(editedUser.firstName === "")) {
        user.firstName = editedUser.firstName;
      }
      if (!(editedUser.lastName === "")) {
        user.lastName = editedUser.lastName;
      }
      if (!(editedUser.email === "")) {
        user.email = editedUser.email;
      }

      bcrypt.hash(plainPassword, saltRounds(), null, function (err, result) {
        if (!(editedUser.password === "")) {
          user.password = result;
        }
        return user.save().then(newUser => {
          reply(user);
        }).catch(err => {
          reply(Boom.badImplementation('error updating user data'));
        });
      });
    });
  },
};

/**
 * Endpoint to delete all users
 */
exports.deleteAll = {
  auth: {
    strategy: 'jwt',
  },

  handler: function (request, reply) {
    User.remove({}).then(err => {
      reply().code(204);
    }).catch(err => {
      reply(Boom.badImplementation('error removing Tweets'));
    });
  },
};

/**
 * Endpoint to authenticate a user
 */
exports.authenticate = {
  auth: false,

  handler: function (request, reply) {
    const user = request.payload;
    User.findOne({ email: user.email }).then(foundUser => {
      if (bcrypt.compareSync(user.password, foundUser.password)) {
        const token = utils.createToken(foundUser);
        reply({ success: true, token: token, user: foundUser }).code(201);
      } else {
        reply({ success: false, message: 'Authentication failed. User not found.' }).code(201);
      }
    }).catch(err => {
      reply(Boom.notFound('internal db failure'));
    });
  },

};

function saltRounds() {
  bcrypt.genSalt(10, function (err, salt) {
    return salt;
  });
}