'use strict';

const User = require('../models/user');
const Data = require('../models/data.json');
const Admin = require('../models/admin.json');
const Joi = require('joi');
const bcrypt = require('bcrypt-nodejs');

/**
 Loads the welcome page
 */
exports.index = {
  auth: false,
  handler: (request, reply) => {
    reply.view('main', {title: 'Welcome to MyTweet'});
  },
};

/**
 Loads the sign up page
 */
exports.signup = {
  auth: false,
  handler: (request, reply) => {
    reply.view('signup', {title: 'Sign up for MyTweet'});
  },
};

/**
 Loads the log in page
 */
exports.login = {
  auth: false,
  handler: (request, reply) => {
    reply.view('login', {title: 'Log in to MyTweet'});
  },
};

/**
 Authenticates a user's login details
 If successful, loads the user's homepage
 If user has administrator password, loads the admin dashboard
 */
exports.authenticate = {
  auth: false,

  validate: {

    payload: {
      email: Joi.string().email().required(),
      password: Joi.string().regex(/^[A-Za-z0-9][^<>]{7,}$/)
    },

    failAction: function (request, reply, source, error) {
      reply.view('login', {
        title: 'Log in error',
        errors: error.data.details,
      }).code(400);
    },
  },

  handler: function (request, reply) {
    const user = request.payload;

    // if user has admin password and registered email address, load admin dashboard
    if (bcrypt.compareSync(user.password, Admin.admin.password)) {
      for (let i = 0; i < Admin.admin.email.length; i++) {
        if (bcrypt.compareSync(user.email, Admin.admin.email[i])) {

          request.cookieAuth.set({
            loggedIn: true,
            loggedInUser: user.email,
            admin: true,
          });
          reply.redirect('/admin');
        } else {
          reply.redirect('/');
          console.log("Unable to find admin account")
        }
      }

      // if log in details valid, load user's homepage
    } else {
      User.findOne({email: user.email}).then(foundUser => {

        if (bcrypt.compareSync(user.password, foundUser.password)) {
          request.cookieAuth.set({
            loggedIn: true,
            loggedInUser: user.email,
          });
          reply.redirect('/home/alltweets');
        } else {
          reply.redirect('/signup');
          console.log("Invalid password")
        }
      }).catch(err => {
        reply.redirect('/');
        console.log("Unable to find user account")
      });
    }
  },
};

/**
 Logs a user out
 */
exports.logout = {
  auth: false,
  handler: function (request, reply) {
    request.cookieAuth.clear();
    reply.redirect('/');
  },
};

/**
 * Registers a new user to the db
 */
exports.register = {
  auth: false,

  validate: {

    payload: {
      firstName: Joi.string().regex(/^[A-Z][a-z][^<>]{1,}$/),
      lastName: Joi.string().regex(/^[A-Z][a-z][^<>]{1,}$/),
      email: Joi.string().email().required(),
      password: Joi.string().regex(/^[A-Za-z0-9][^<>]{7,}$/)
    },

    failAction: function (request, reply, source, error) {
      reply.view('signup', {
        title: 'Sign up error',
        errors: error.data.details,
      }).code(400);
    },
  },
  handler: function (request, reply) {
    const user = new User(request.payload);
    const plainPassword = user.password;

    bcrypt.hash(plainPassword, saltRounds(), null, function (err, result) {
      user.password = result;
      console.log(result);
      return user.save().then(newUser => {
        reply.redirect('/login');
      }).catch(err => {
        reply.redirect('/');
      });
    });
  }
};

/**
 * Displays the settings page
 */
exports.viewSettings = {
  handler: function (request, reply) {
    const userEmail = request.auth.credentials.loggedInUser;
    User.findOne({email: userEmail}).then(foundUser => {
      reply.view('settings', {title: 'Edit Account Settings', user: foundUser});
    }).catch(err => {
      reply.redirect('/');
    });
  },
};

/**
 * Updates a user's account settings
 */
exports.updateSettings = {

  validate: {

    payload: {
      firstName: Joi.string().regex(/^[A-Z][a-z][^<>]{1,}$/),
      lastName: Joi.string().regex(/^[A-Z][a-z][^<>]{1,}$/),
      email: Joi.string().email().required(),
    },

    failAction: function (request, reply, source, error) {
      reply.view('settings', {
        title: 'Settings adjust error',
        errors: error.data.details,
      }).code(400);
    },
  },

  handler: function (request, reply) {

    const loggedInUserEmail = request.auth.credentials.loggedInUser;
    const editedUser = request.payload;
    const plainPassword = editedUser.password;
    User.findOne({email: loggedInUserEmail}).then(user => {
      user.firstName = editedUser.firstName;
      user.lastName = editedUser.lastName;
      user.email = editedUser.email;
      return user.save();
    }).then(user => {
      reply.redirect('/home/alltweets');
    }).catch(err => {
      reply.redirect('/');
    });
  },
};

/**
 * Creates a following between one user and another
 */
exports.follow = {

  handler: function (request, reply) {

    const loggedInUserEmail = request.auth.credentials.loggedInUser;
    const followingId = request.payload.id;

    // Checks for pre-existing following between users, & that the users are different
    User.findOne({ email: loggedInUserEmail }).then(follower => {
      if (checkFollowers(followingId, follower.following) && (followingId != follower._id)) {
        reply.redirect('/home/alltweets');
        console.log('Error: Cannot duplicate a following')
      } else {

        User.findOne({ _id: followingId }).then(user => {
          user.followers.push(follower._id);
          follower.following.push(followingId);
          follower.save();
          user.save();
          reply.redirect('/home/alltweets');
        }).catch(err => {
          reply.redirect('/');
          console.log('Error establishing a following')
        });
      }
    }).catch(err => {
      reply.redirect('/');
      console.log('Error: Can not create a following')
    });
  },
};

function saltRounds() {
  bcrypt.genSalt(10, function (err, salt) {
    return salt;
  });
}

function checkFollowers(followingId, followerList) {
  for (let i = 0; i < followerList.length; i++) {
    if (followingId == followerList[i]) {
      return true;
    }
  }
  return false;
}
