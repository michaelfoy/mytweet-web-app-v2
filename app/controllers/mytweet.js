'use strict';
const Tweet = require('../models/tweet');
const User = require('../models/user');
const Joi = require('joi');

/**
 * Displays the homepage
 */
exports.home = {
  handler: function (request, reply) {
    let foundUser;
    const userEmail = request.auth.credentials.loggedInUser;
    const option = request.params.option;
    let followingList;
    let tweetList;
    let followersArray;

    User.findOne({email: userEmail}).then(user => {
      followersArray = getFollowers(user);
      return foundUser = user;
    }).then(user => {
      if(option === "following") {
        followingList = user.following;
        tweetList = getFollowingTweets(followingList);

        tweetList.sort(function (a, b) {
          if (a.date > b.date) {
            return -1;
          }
          if (a.date < b.date) {
            return 1;
          }
          return 0;
        });

        reply.view('home', {
          title: 'MyTweet',
          tweets: tweetList,
          general: true,
          user: foundUser,
          mytweets: false,
          followers: followersArray,
        });

      } else if(option === "mytweets") {
        const id  = user._id.toString();
        tweetList = getMyTweets(id);
        tweetList.sort(function (a, b) {
          if (a.date > b.date) {
            return -1;
          }
          if (a.date < b.date) {
            return 1;
          }
          return 0;
        });

        reply.view('home', {
          title: 'MyTweet',
          tweets: tweetList,
          general: true,
          user: foundUser,
          mytweets: true,
          followers: followersArray,
        });

      } else {
        Tweet.find({}).populate('tweeter').then(tweets => {
          tweets.sort(function (a, b) {
            if (a.date > b.date) {
              return -1;
            }
            if (a.date < b.date) {
              return 1;
            }
            return 0;
          });

          reply.view('home', {
            title: 'MyTweet',
            tweets: tweets,
            general: true,
            user: foundUser,
            mytweets: false,
            followers: followersArray,
          });
        }).catch(err => {
          console.log("Unable to get all tweets from db");
          reply.redirect('/');
        });
      }
    }).catch(err => {
      console.log("Unable to get logged in user");
      reply.redirect('/');
    });
  },
};

/**
 * Displays an individual profile page,
 */
exports.profilepage = {
  handler: function (request, reply) {

    let userEmail = request.params.email;
    let foundUser;
    let followersArray;
    let name;

    // Gets a user, their followers and their tweets
    User.findOne({email: userEmail}).then(user => {
      name = user.firstName;
      return foundUser = user;
    }).then(user => {
      return followersArray = getFollowers(foundUser);
    }).then(user => {
      return Tweet.find({tweeter: foundUser._id})
    }).then(userTweets => {

      // Sorts a user's tweets
      userTweets.sort(function (a, b) {
        if (a.date > b.date) {
          return -1;
        }
        if (a.date < b.date) {
          return 1;
        }
        return 0;
      });

      reply.view('profilepage', {
        title: 'MyTweet Profile Page',
        tweets: userTweets,
        user: foundUser,
        followers: followersArray,
      });
    }).catch(err => {
      console.log("Unable to get logged in user tweets");
      reply.redirect('/');
    });
  },
};

/**
 * Creates a new tweet, returns to homepage
 */
exports.newTweet = {

  payload: {
    parse: true,
    output: 'data',
  },

  handler: (request, reply) => {
    let data = request.payload;

    // Checks that the tweet contains between 1 and 140 characters and contains no script tags
    if ((data['content'].length >= 1) && (data['content'].length <= 140)
        && !data['content'].includes('<script>')) {

      const userEmail = request.auth.credentials.loggedInUser;
      User.findOne({email: userEmail}).then(user => {

        let tweet = new Tweet(data);
        tweet.tweeter = user._id;
        tweet.date = getDate();

        // Checks for and saves a tweet image
        if (data.picture.length > 0) {
          tweet.picture.data = data.picture;
        }
        return tweet.save();

      }).then(newTweet => {
        reply.redirect('/home/alltweets');
      }).catch(err => {
        console.log("Internal error");
        reply.redirect('/');
      });
    } else {
      reply.redirect('/home/alltweets');
    }
  },
};

/**
 * Deletes selected tweets
 */
exports.delete = {
  handler: function (request, reply) {
    const userEmail = request.params.email;
    const data = request.payload;
    console.log(data);
    const tweetsArray = Object.keys(data);
    console.log(tweetsArray);

    for (let i = 0; i < tweetsArray.length; i++) {
      Tweet.findByIdAndRemove(tweetsArray[i], function (err) {
        if (err) throw err;
        console.log('Tweet deleted: ' + tweetsArray[i]);
      });
    }
    reply.redirect('/profilepage/' + userEmail);
  }
};

/**
 * Returns a tweet image to the page
 */
exports.getPicture = {
  handler: function (request, reply) {
    const id = request.params.id;
    Tweet.findOne({_id: id}).then(tweet => {
      reply(tweet.picture.data).type('image');
    });
  }
};

/**
 * Returns the current date and time as a string
 */
function getDate() {
  let date = new Date();
  let dd = date.getDate();
  let mm = date.getMonth() + 1;
  let hh = date.getHours();
  let min = date.getMinutes();
  const yy = date.getFullYear();

  const nums = [mm, dd, hh, min];
  for (let i = 0; i < nums.length; i++) {
    if (nums[i] < 10) {
      nums[i] = '0' + nums[i]
    }
  }
  return nums[1] + '/' + nums[0] + '/' + yy + ' - ' + nums[2] + ':' + nums[3];
}

/**
 * Returns an array of the user's followers
 */
function getFollowers(user) {
  let followersArray = [];

  if( user.followers.length > 0) {
    for (let i = 0; i < user.followers.length; i++) {
      User.findOne({_id: user.followers[i]}).then(user => {
        followersArray.push({follower: user});
      });
    }
  }
  return followersArray;
}

/**
 * Returns a list of tweets from users being followed
 */
function getFollowingTweets(followingList) {
  let tweetList = [];

  for (let i = 0; i < followingList.length; i++) {

    Tweet.find({tweeter: followingList[i]}).populate('tweeter').then(tweets => {
      for (let i = 0; i < tweets.length; i++) {
        tweetList.push(tweets[i]);
      }
    }).catch(err => {
      console.log("Unable to get following tweets from db");
      reply.redirect('/');
    });
  }
  return tweetList;
}

function getMyTweets(id) {
  let tweetList = [];

  Tweet.find({ tweeter: id }).populate('tweeter').then(tweets => {
    for (let i = 0; i < tweets.length; i++) {
      tweetList.push(tweets[i]);
    }
  }).catch(err => {
    console.log("Unable to get users tweets from db");
    reply.redirect('/');
  });
  return tweetList;
}