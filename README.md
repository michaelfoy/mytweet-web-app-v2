# ICTSkills Enterprise Web Development (2017)
# Assignment 2: MyTweet

##### Student name: Michael Foy
##### Student Number: 06695779
##### Date: 09/01/2017

## Description of Application:
This assignment comprises a Twitter-style application for users to post publicly viewable messages. Once a user has registered their details by creating an account they can do the following:
- View the public feed of all messages from all users
- View a feed of another user's messages
- View a feed of one’s own messages
- Follow another user
- View a feed of posts from followed users
- Post messages to the public feed
- Include images in their post
- Delete one’s own messages by selection, or for all messages
- Adjust their name and email settings
- Logout

The app also includes an administrator dashboard with the following features:
- Register a new user
- Delete user by selection or in bulk
- Delete a user’s tweets by selection or in bulk


### Log in details:
#### Preloaded users:
User emails: homer@simpson.com, marge@simpson.com, gary@edams.com, bart@simpson.com
All users have the same password: __bigsecret__
#### Admin:
To log in to administrator dashboard, use monty@burns.com with the password: __adminsecret__
## Hosting
Repository for this app at the following:
- [BitBucket](https://bitbucket.org/michaelfoy/mytweet-android-app-v2)

The app has been deployed on an AWS instance which is linked to a mongo database hosted by mlab.com. It can be opened with the following ip address:
- http://35.166.75.56:4000/

### Instructions:
- Using the command: git clone https://michaelfoy@bitbucket.org/michaelfoy/mytweet-web-app-v2.git, download a local version of the app.
- To run locally, a mongodb installation must be installed and running.
- The app's database is pre-configured to point to localhost, running on port 4000.
- Within the folder mytweet-web-app-v2, use npm from the commandline to install the app's dependencies.
- The following dependencies are necessary: bcrypt-nodejs, boom, handlebars, hapi, hapi-auth-cookie, hapi-auth-jwt2, inert, joi, jsonwebtoken, lodash, mocha, mongoose, mongoose-seeder, vision
- Finally within an IDE, configure a node.js execution to point to file index.js
- With these complete, the app should run successfully