const Accounts = require('./app/controllers/accounts.js');
const MyTweet = require('./app/controllers/mytweet.js');
const Admin = require('./app/controllers/admin.js');
const Assets = require('./app/controllers/assets.js');

module.exports = [

  { method: 'GET', path: '/', config: Accounts.index },
  { method: 'GET', path: '/signup', config: Accounts.signup },
  { method: 'GET', path: '/login', config: Accounts.login },
  { method: 'POST', path: '/authenticate', config: Accounts.authenticate },
  { method: 'GET', path: '/logout', config: Accounts.logout },
  { method: 'POST', path: '/register', config: Accounts.register },
  { method: 'GET', path: '/viewSettings', config: Accounts.viewSettings },
  { method: 'POST', path: '/updateSettings', config: Accounts.updateSettings },
  { method: 'POST', path: '/follow', config: Accounts.follow },


  { method: 'GET', path: '/home/{option}', config: MyTweet.home },
  { method: 'POST', path: '/newTweet', config: MyTweet.newTweet },
  { method: 'GET', path: '/profilepage/{email}', config: MyTweet.profilepage },
  { method: 'POST', path: '/deleteTweets/{email}', config: MyTweet.delete },
  { method: 'GET', path: '/getPicture/{id}', config: MyTweet.getPicture },

  { method: 'GET', path: '/admin', config: Admin.home },
  { method: 'POST', path: '/deleteUser', config: Admin.deleteuser },
  { method: 'POST', path: '/adminregister', config: Admin.register },
  { method: 'POST', path: '/selectUser', config: Admin.selectUser },
  { method: 'POST', path: '/admindeletetweets', config: Admin.deletetweets },

  { method: 'GET', path: '/{param*}', config: { auth: false }, handler: Assets.servePublicDirectory, },

];