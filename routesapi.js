const UsersApi = require('./app/api/usersapi');
const TweetsApi = require('./app/api/tweetsapi');

module.exports = [
  { method: 'GET', path: '/api/users', config: UsersApi.getAllUsers },
  { method: 'GET', path: '/api/users/{id}', config: UsersApi.getOne },
  { method: 'POST', path: '/api/users', config: UsersApi.create },
  { method: 'POST', path: '/api/users/{id}', config: UsersApi.update },
  { method: 'POST', path: '/api/users/del', config: UsersApi.delete },
  { method: 'POST', path: '/api/users/authenticate', config: UsersApi.authenticate },

  { method: 'POST', path: '/api/users/{id}/tweets', config: TweetsApi.create },
  { method: 'GET', path: '/api/users/{id}/tweets', config: TweetsApi.getTweetsforUser },
  { method: 'POST', path: '/api/users/tweet', config: TweetsApi.getOneTweet },
  { method: 'POST', path: '/api/tweets', config: TweetsApi.delete },
  { method: 'GET', path: '/api/tweets', config: TweetsApi.findAll },

  { method: 'DELETE', path: '/api/delTweets', config: TweetsApi.deleteAll },
  { method: 'DELETE', path: '/api/delUsers', config: UsersApi.deleteAll }
];