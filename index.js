'use strict';

const Hapi = require('hapi');
const fs = require('fs');
const data = require('./app/models/admin.json');
const utils = require('./app/api/utils.js');

/*var options = {
  port: 4443,
  tls: {
    key: fs.readFileSync('private/server.key'),
    cert: fs.readFileSync('private/server.crt')
  }
};*/

var server = new Hapi.Server();
require('./app/models/db');

server.connection({ port: process.env.PORT || 4000 });
// options

server.register([require('inert'), require('vision'), require('hapi-auth-cookie'), require('hapi-auth-jwt2')], err => {

  if (err) {
    throw err;
  }

  server.views({
    engines: {
      hbs: require('handlebars'),
    },
    relativeTo: __dirname,
    path: './app/views',
    layoutPath: './app/views/layouts',
    partialsPath: './app/views/partials',
    layout: true,
    isCached: false,
  });

  server.auth.strategy('standard', 'cookie', {
    password: data.authPassword,
    cookie: 'mytweet-cookie',
    redirectTo: '/login',
    isSecure: false,
    ttl: 24 * 60 * 60 * 1000,
  });

  server.auth.strategy('jwt', 'jwt', {
    key: data.authPassword,
    validateFunc: utils.validate,
    verifyOptions: { algorithms: ['HS256'] },
  });

  server.auth.default({
    strategy: 'standard',
  });

  server.route(require('./routes'));
  server.route(require('./routesapi'));

  server.start((err) => {
    if (err) {
      throw err;
    }

    console.log('Server listening at:', server.info.uri);
  });
});