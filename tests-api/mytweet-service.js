'use strict';

const SyncHttpService = require('./sync-http-service');

/**
 * Class to assist routing from test suite to api
 */
class MyTweetService {

  constructor(baseUrl) {
    this.httpService = new SyncHttpService(baseUrl);
  }

  getAllUsers() {
    return this.httpService.get('/api/users');
  }

  getUser(id) {
    return this.httpService.get('/api/users/' + id);
  }

  createUser(newUser) {
    return this.httpService.post('/api/users', newUser);
  }

  updateUserSettings(id, data) {
    return this.httpService.post('/api/users/' + id, data);
  }

  deleteUsers(data) {
    return this.httpService.deleteSelected('/api/users/del', data);
  }

  createTweet(userId, tweet) {
    return this.httpService.post('/api/users/' + userId + '/tweets', tweet);
  }

  getTweetsforUser(id) {
    return this.httpService.get('/api/users/' + id + '/tweets');
  }

  getOneTweet(id) {
    return this.httpService.post('/api/users/tweet', id)
  }

  deleteTweetsForUser(data) {
    return this.httpService.deleteSelected('/api/tweets', data)
  }

  getAllTweets() {
    return this.httpService.get('/api/tweets')
  }

  deleteAllTweets() {
    return this.httpService.delete('/api/delTweets')
  }

  deleteAllUsers() {
    return this.httpService.delete('/api/delUsers')
  }

  authenticate(user) {
    return this.httpService.post('/api/users/authenticate', user);
  }

  login(user) {
    return this.httpService.setAuth('/api/users/authenticate', user);
  }

  logout() {
    this.httpService.clearAuth();
  }
}

module.exports = MyTweetService;