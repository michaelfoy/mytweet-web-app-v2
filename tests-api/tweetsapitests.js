'use strict';

const assert = require('chai').assert;
const TweetService = require('./mytweet-service');
const fixtures = require('./fixtures.json');
const _ = require('lodash');

/**
 Test suite for tweetsapi.js.
 Before and after each test all tweets and users are deleted.
 */
suite('MyTweet API tests', function () {

  let tweets = fixtures.tweets;

  const tweetService = new TweetService(fixtures.tweetService);
  const newUser = fixtures.newUser;
  const newUser2 = fixtures.newUser2;
  const terminalUser = fixtures.terminalUser;
  const users = fixtures.users;

  beforeEach(function () {

    // Login and deletion to clear seeded database
    tweetService.login(users[0]);
    tweetService.deleteAllTweets();
    tweetService.deleteAllUsers();
    tweetService.logout();
  });

  afterEach(function () {

    //Login and deletion of dud user to clear test data
    tweetService.createUser(terminalUser);
    tweetService.login(terminalUser);
    tweetService.deleteAllTweets();
    tweetService.deleteAllUsers();
    tweetService.logout();
  });

  /**
   * Tests the successful creation of a tweet
   */
  test('create a tweet', function () {
    const returnedUser = tweetService.createUser(newUser);
    tweetService.login(newUser);

    tweetService.createTweet(returnedUser._id, tweets[0]);

    const returnedTweets = tweetService.getAllTweets();
    assert.equal(returnedTweets.length, 1);
    assert(_.some([returnedTweets[0]], tweets[0]), 'returned tweet must be a superset of tweet');
  });

  /**
   * Tests retrieval of all tweets created by a single user
   */
  test('get all tweets for a user', function (){
    const returnedUser = tweetService.createUser(newUser);
    tweetService.login(newUser);

    for (let i = 0; i < tweets.length; i++) {
      tweetService.createTweet(returnedUser._id, tweets[i]);
    }

    const returnedTweets = tweetService.getTweetsforUser(returnedUser._id);
    assert.equal(returnedTweets.length, tweets.length);
    for (let i = 0; i < tweets.length; i++) {
      assert(_.some([returnedTweets[i]], tweets[i]), 'returned tweet must be a superset of tweet');
    }
  });

  /**
   * Tests retrieval of a single tweet
   */
  test('get one tweet', function () {
    const returnedUser = tweetService.createUser(newUser);
    tweetService.login(newUser);

    const newTweetId = tweetService.createTweet(returnedUser._id, tweets[0]);
    const returnedTweet = tweetService.getOneTweet(newTweetId);

    assert.equal(returnedTweet[0]._id, newTweetId)
  });

  /**
   * Tests an attempt to retrieve an invalid tweet
   */
  test('get invalid tweet', function () {
    const returnedUser = tweetService.createUser(newUser);
    tweetService.login(newUser);

    const nullTweet = tweetService.getOneTweet('1234');
    assert.isNull(nullTweet);
    const nullTweet2 = tweetService.getOneTweet('01234567890123456789');
    assert.isNull(nullTweet2);
  });

  /**
   * Tests retrieval of all tweets stored in the database
   */
  test('get all tweets', function () {
    const returnedUser = tweetService.createUser(newUser);
    const returnedUser2 = tweetService.createUser(newUser2);
    tweetService.login(newUser);

    for (let i = 0; i < tweets.length; i++) {
      tweetService.createTweet(returnedUser._id, tweets[i]);
    }

    for (let i = 0; i < tweets.length; i++) {
      tweetService.createTweet(returnedUser2._id, tweets[i]);
    }

    let returnedTweets = tweetService.getAllTweets();
    assert.equal(returnedTweets.length, (tweets.length * 2));
  });

  /**
   * Tests deletion of all tweets created by a single user
   */
  test('delete tweets for a user', function () {
    const returnedUser = tweetService.createUser(newUser);
    const returnedUser2 = tweetService.createUser(newUser2);
    tweetService.login(newUser);
    let deletionData = {};

    for (let i = 0; i < tweets.length; i++) {
      tweetService.createTweet(returnedUser._id, tweets[i]);
    }

    for (let i = 0; i < tweets.length; i++) {
      tweetService.createTweet(returnedUser2._id, tweets[i]);
    }

    let returnedTweets = tweetService.getTweetsforUser(returnedUser._id);
    let returnedTweets2 = tweetService.getTweetsforUser(returnedUser2._id);

    assert.equal(returnedTweets.length, tweets.length);
    assert.equal(returnedTweets2.length, tweets.length);

    for (let i = 0; i < returnedTweets.length; i++) {
      deletionData[returnedTweets[i]._id] = "tweetforDeletion";
    }
    tweetService.deleteTweetsForUser(deletionData);

    let secondReturnedTweets = tweetService.getTweetsforUser(returnedUser._id);
    let secondReturnedTweets2 = tweetService.getTweetsforUser(returnedUser2._id);

    assert.equal(secondReturnedTweets.length, 0);
    assert.equal(secondReturnedTweets2.length, tweets.length);
  });

  /**
   * Tests deletion of a selection of tweets from a user
   */
  test('delete selection of tweets for user', function () {
    const returnedUser = tweetService.createUser(newUser);
    tweetService.login(newUser);
    let deletionData = {};

    for (let i = 0; i < tweets.length; i++) {
      tweetService.createTweet(returnedUser._id, tweets[i]);
    }

    const returnedTweets = tweetService.getTweetsforUser(returnedUser._id);
    assert.equal(returnedTweets.length, tweets.length);

    for (let i = 0; i < 2; i++) {
      deletionData[returnedTweets[i]._id] = "tweetforDeletion";
    }
    tweetService.deleteTweetsForUser(deletionData);

    const returnedTweets2 = tweetService.getTweetsforUser(returnedUser._id);
    assert.equal(returnedTweets2.length, (tweets.length - 2));
  });
});