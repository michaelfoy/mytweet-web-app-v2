'use strict';

const assert = require('chai').assert;
const MyTweetService = require('./mytweet-service');
const fixtures = require('./fixtures.json');
const utils = require('../app/api/utils.js');
const _ = require('lodash');

/**
Test suite for usersapi.js.
Before and after each test all tweets and users are deleted.
 */
suite('User API tests', function () {

  let users = fixtures.users;
  let newUser = fixtures.newUser;
  let newUser2 = fixtures.newUser2;
  const terminalUser = fixtures.terminalUser;

  const tweetService = new MyTweetService(fixtures.tweetService);

  beforeEach(function () {

    // Login and deletion to clear seeded database
    tweetService.login(users[0]);
    tweetService.deleteAllTweets();
    tweetService.deleteAllUsers();
    tweetService.logout();
  });

  afterEach(function () {

    //Login and deletion of dud user to clear test data
    tweetService.createUser(terminalUser);
    tweetService.login(terminalUser);
    tweetService.deleteAllTweets();
    tweetService.deleteAllUsers();
    tweetService.logout();
  });

  /**
   * Tests login & logout functionality
   */
  test('login-logout', function () {
    let returnedUsers = tweetService.getAllUsers();
    assert.isNull(returnedUsers);

    const returnedUser = tweetService.createUser(newUser);
    tweetService.login(newUser);
    returnedUsers = tweetService.getAllUsers();
    assert.isNotNull(returnedUsers);

    tweetService.logout();
    returnedUsers = tweetService.getAllUsers();
    assert.isNull(returnedUsers);
  });

  /**
   * Tests creation of a new user
   */
  test('create a user', function () {
    const returnedUser = tweetService.createUser(newUser);
    returnedUser.password = newUser.password;
    assert(_.some([returnedUser], newUser), 'returnedCandidate must be a superset of newCandidate');
    assert.isDefined(returnedUser._id);
  });

  /**
   * Tests retrieval of all users stored in the database
   */
  test('get all users', function () {
    for (let i = 0; i < users.length; i++) {
      tweetService.createUser(users[i]);
    }
    users[0].password = newUser.password;
    tweetService.login(users[0]);
    assert.equal(tweetService.getAllUsers().length, users.length);
  });

  /**
   * Tests retrieval of one user from the database
   */
  test('get one user', function () {
    const returnedUser = tweetService.createUser(newUser);
    tweetService.login(newUser);
    const returnedUser2 = tweetService.getUser(returnedUser._id);
    assert.deepEqual(returnedUser, returnedUser2);
  });

  /**
   * Tests retrieval of an invalid user
   */
  test('get invalid user', function () {
    const testUser = tweetService.createUser(newUser);
    tweetService.login(newUser);

    const returnedUser = tweetService.getUser('1234');
    assert.isNull(returnedUser);
    const returnedUser2 = tweetService.getUser('01234567890123456789');
    assert.isNull(returnedUser2);
  });

  /**
   * Tests deletion of a single user
   */
  test('delete a user', function () {
    const returnedUser = tweetService.createUser(newUser);
    const returnedUser2 = tweetService.createUser(newUser2);
    tweetService.login(newUser2);

    assert(tweetService.getUser(returnedUser._id) != null);
    assert(tweetService.getUser(returnedUser2._id) != null);

    let deletionData = {};
    deletionData[returnedUser._id] = "userForDeletion";

    tweetService.deleteUsers(deletionData);
    assert.equal(tweetService.getAllUsers().length, 1);
  });

  /**
   * Tests in detail the comparison between database and test data
   */
  test('get users detail', function () {
    for (let i of users) {
      tweetService.createUser(i);
    }
    users[0].password = newUser.password;
    tweetService.login(users[0]);

    const allUsers = tweetService.getAllUsers();

    for (var i = 0; i < users.length; i++) {
      allUsers[i].password = users[i].password;
    }

    for (var i = 0; i < users.length; i++) {
      assert(_.some([allUsers[i]], users[i]), 'returnedUser must be a superset of newUser');
    }
  });

  /**
   * Tests modification of a user's profile details
   */
  test('update user settings', function () {
    const testUser = tweetService.createUser(newUser);
    tweetService.login(newUser);
    const data = fixtures.newUser2;

    const returnedUser = tweetService.getUser(testUser._id);
    assert.equal(returnedUser.firstName, newUser.firstName);
    assert.equal(returnedUser.lastName, newUser.lastName);
    assert.equal(returnedUser.email, newUser.email);

    tweetService.updateUserSettings(returnedUser._id, data);

    const returnedUser2 = tweetService.getUser(testUser._id);
    assert.equal(returnedUser2.firstName, newUser2.firstName);
    assert.equal(returnedUser2.lastName, newUser2.lastName);
    assert.equal(returnedUser2.email, newUser2.email);
  });

  test('authenticate a user', function() {
    const returnedUser = tweetService.createUser(newUser);
    const response = tweetService.authenticate(newUser);
    assert(response.success);
    assert.isDefined(response.token);
  });

  test('verify Token', function () {
    const returnedUser = tweetService.createUser(newUser);
    const response = tweetService.authenticate(newUser);

    const userInfo = utils.decodeToken(response.token);
    assert.equal(userInfo.email, returnedUser.email);
    assert.equal(userInfo.userId, returnedUser._id);
  });
});